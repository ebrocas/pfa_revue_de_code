#!/usr/bin/env python3
# coding: utf-8
# Copyright (C) 2019 Eloïse Brocas, Mert Ernez, Jérôme Faure, Raphaël Gilliot
# Lucas Henry, Yann Julienne, Benjamin Le Rohellec
#
# This file is part of coderev, a code review tool.
#
# coderev is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# coderev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with coderev.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import logging
from os import getcwd

import coderev_add
import coderev_display
from src import conversion
from src.updater import AnchorUpdater
from src.versionning import FileVersionProvider, detect_version_system, parse_ref

logging.basicConfig(level=logging.DEBUG)


def create_subparser_update(subparsers):
    """
        Add a subparser for updating annotations after changes in code files.

        :param subparsers: the parser in which the subparser will be added
    """
    parser_update = subparsers.add_parser(
        "update", help="Update the place of the annotations after changes in the file."
    )
    parser_update.add_argument("--files", metavar="F", action="append")
    parser_update.add_argument(
        "-s",
        "--strategies",
        metavar="N",
        action="append",
        help='List of strategies to use. Choose from "leven", "simple"',
    )
    parser_update.add_argument(
        "-d", "--dry-run", action="store_true", help="Do not actually modify anchors."
    )


def create_subparser_cat(subparsers):
    """
        Add a subparser for displaying annotations and text together

        :param subparsers: the parser in which the subparser will be added
    """
    parser_cat = subparsers.add_parser(
        "cat", help="display the file content and annotations together."
    )
    parser_cat.add_argument(
        "--version",
        "-v",
        type=str,
        help="Select a different version from the current version",
    )
    parser_cat.add_argument("file")


def create_subparser_edit(subparsers):
    """
       Add a subparser for editing annotations, references and anchors.

       :param subparsers: the parser in which the subparser will be added
    """
    parser_edit = subparsers.add_parser("edit", help="edit an object.")
    subparser_edit = parser_edit.add_subparsers(dest="edit", title="Edit")

    parser_edit_annotation = subparser_edit.add_parser("annotation")
    parser_edit_anchor = subparser_edit.add_parser("anchor")

    parser_edit_annotation.add_argument("id", type=int, help="Id of the annotation")
    parser_edit_annotation.add_argument(
        "--title", type=str, help="Title for the annotation"
    )
    parser_edit_annotation.add_argument(
        "--tags", nargs="+", type=str, help="List of tags for the annotation"
    )
    parser_edit_annotation.add_argument(
        "--description", type=str, help="Description for the annotation"
    )
    parser_edit_annotation.add_argument(
        "--ref",
        nargs="+",
        metavar="hash",
        type=int,
        help="Identifiers of anchors to use.",
    )

    parser_edit_anchor.add_argument("id", type=int, help="Edit the anchor")
    parser_edit_anchor.add_argument("--file", type=str, help="File of the anchor")
    parser_edit_anchor.add_argument("--line", type=str, help="Line of the anchor")


def create_parser():
    """ :return: a parser for the command line """
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    subparsers = parser.add_subparsers(dest="action", title="Actions")
    parser_init = subparsers.add_parser("init", help="Init a new working directory.")
    coderev_add.create_subparser_add(
        subparsers.add_parser(
            "add", help="Add an annotation or an anchor to the database."
        )
    )
    coderev_display.create_subparser_display(
        subparsers.add_parser(
            "display",
            help="Display informations about annotations and anchors. Filter can be applied.",
        )
    )
    create_subparser_update(subparsers)
    create_subparser_cat(subparsers)
    create_subparser_edit(subparsers)

    parser.add_argument(
        "--version-dirs",
        nargs=2,
        metavar=("old_dir", "new_dir"),
        default=None,
        dest="version_dirs",
        help="You can specify two directories: the old version and the new."
        "If so, the files in the old directory are considered to be the old version of the file, "
        "with the newer version in the second directory."
        "If not, a file version manager is required to manage versions.",
    )
    return parser


if __name__ == "__main__":
    parser = create_parser()
    args = parser.parse_args()

    if args.version_dirs is not None:
        version_manager = FileVersionProvider(*args.version_dirs)
    else:
        version_manager = detect_version_system()

    if version_manager is None:
        print("No version manager detected.")
        print("A version manager is required to synchronize data.")
        exit(1)

    if args.action == "init":
        conversion.init_directory(getcwd())

    elif args.action == "add":
        coderev_add.execution(args, version_manager)

    elif args.action == "display":
        coderev_display.execution(args, version_manager)

    elif args.action == "cat":
        anchors = conversion.load_anchors(parse_ref(version_manager, args.version))
        annotations = conversion.load_annotations()
        annotations.cat_annotations(
            anchors,
            version_manager,
            parse_ref(version_manager, args.version),
            args.file,
            only_context=False,
        )

    elif args.action == "update":
        current_commit = version_manager.actual_version()
        previous_commit = version_manager.previous_version()
        anchors = conversion.load_anchors(previous_commit)
        updater = AnchorUpdater(
            anchors, version_manager, args.strategies, should_save=(not args.dry_run)
        )
        updater.update((previous_commit, current_commit), auto=False)
    elif args.action == "edit":
        if args.edit == "annotation":
            anchors = conversion.load_anchors(version_manager.actual_version())
            annotations = conversion.load_annotations()

            annotation = annotations.get(args.id)
            annotation.edit(args.ref, args.title, args.description, args.tags)

            conversion.save_anchors(anchors, version_manager.actual_version())
            conversion.save_annotations(annotations)
        elif args.edit == "anchor":
            anchors = conversion.load_anchors(version_manager.actual_version())
            anchor = anchors.get_by_id(args.id)
            anchor.edit(args.file, args.line)
            conversion.save_anchors(anchors, version_manager.actual_version())
    else:
        parser.print_help()
