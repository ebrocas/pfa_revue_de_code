# coding:utf8
from json import load, dump
from os.path import exists
from typing import Generic, TextIO, Optional, TypeVar, Union

T = TypeVar('T', str, int, float, complex, list, dict)


class ConfigValue(Generic[T]):
    """ Store all the informations about a configuration key.
    The saved and imported datas must be stored in this object.
    
    After creating a ConfigValue, register it in a Configuration object.
    You can now import and save the whole configuration file.
    
    key and default value are set during the initialisation of the ConfigValue and must not be modified.
    When you change the real value of the configuration, please save the Configuration parent."""
    
    def __init__(self, key: str, default: T, comment: str = None):
        self._key = key
        self._default = default
        self.value = default
        self.comment = comment
        
    def get_key(self) -> str:
        return self._key
        
    def get_value(self) -> T:
        return self.value
    
    def get_default(self) -> T:
        return self._default
    
    def set_value(self, v: T) -> None:
        """ Change the register value. Doesn't save the new value into a file.
        Must be overrided to check if the new value is correct and set a correct value."""
        self.value = v
        

class ConfigValueFloat(ConfigValue[float]):
    """ Example of a value restricted between a min and a max value. """
    
    def __init__(self, key: str, default: float, comment: str = None, min: float = None, max: float = None):
        super(ConfigValueFloat, self).__init__(key, default, comment)
        self.min = min
        self.max = max
        
    def set_value(self, v: float) -> None:
        if self.min is not None and v < self.min:
            super(ConfigValueFloat, self).set_value(self.min)
        elif self.max is not None and v > self.max:
            super(ConfigValueFloat, self).set_value(self.max)
        else:
            super(ConfigValueFloat, self).set_value(v)


class Configuration:
    """ This object represent a configuration file.
    ConfigValue must be registered to this object before loading and saving data.
    
    A file (str) can be associated to the configuration, to help loading and saving data.
    """
    
    def __init__(self, file: Optional[str] = None):
        self._values = dict()
        self.file = file
        
    def keys(self):
        """:return: the list of all keys registered."""
        return self._values.keys()
        
    def register_value(self, v: ConfigValue):
        if v.get_key() in self.keys():
            raise KeyError("The key '%s' is already used !" % v.get_key())
        self._values[v.get_key()] = v
        
    def save(self, file: Union[TextIO, str] = None):
        """ Save the configurations into the opened file.
        If file is a string, it can be automatically opened and written. May fail if the parent directory doesn't exist.
        If file is None, the file associated with the configuration will be used.
        :param file: an io object where the datas must be saved."""
        if file is None:
            if self.file is None:
                raise KeyError("A file is required to save the data !")
            file = self.file

        if isinstance(file, str):
            with open(file, "w") as f:
                self.save(f)
                return
        
        datas = dict()
        
        for config in self._values.values():
            parent = datas
            subsections = config.get_key().split(".")
            
            while len(subsections) > 1:
                subkey = subsections.pop(0)
                if subkey in parent.keys():
                    parent = parent.get(subkey)
                else:
                    parent[subkey] = dict()
                    parent = parent.get(subkey)
            
            parent[subsections[0]] = config.get_value()
            if config.comment is not None:
                parent[subsections[0] + "__comment"] = config.comment
            
        dump(datas, file, indent=4, sort_keys=True)
        
    def load(self, file: Union[TextIO, str] = None):
        """ Load the configuration values from the opened file.
        Only the registered ConfigValue are loaded, any unknown key will be ignored and remove during saving.
        If the file is a string, and the file exists, it can be automatically opened and read.
        If file is None, the file associated with the configuration will be used.
        :param file: an io object from where the datas can be loaded."""
        if file is None:
            if self.file is None:
                raise KeyError("A file is required to save the data !")
            file = self.file
            
        if isinstance(file, str):
            if not exists(file):
                return
            with open(file, "r") as f:
                self.load(f)
        else:
            datas = load(file)
            name = list()
            self._load_group(name, datas)
        
    def _load_group(self, name: list, datas: dict):
        for key, value in datas.items():
            name.append(key)
            
            if isinstance(value, dict):
                self._load_group(name, value)
            elif key.endswith("__comment"):
                pass
            elif ".".join(name) in self.keys():
                config_value = self._values[".".join(name)]
                config_value.set_value(value)
            else:
                print("Unknow key:", ".".join(name))
                
            name.pop(-1)
