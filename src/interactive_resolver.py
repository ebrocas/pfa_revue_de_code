# Copyright (C) 2019 Eloïse Brocas, Mert Ernez, Jérôme Faure, Raphaël Gilliot
# Lucas Henry, Yann Julienne, Benjamin Le Rohellec
#
# This file is part of coderev, a code review tool.
#
# coderev is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# coderev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with coderev.  If not, see <https://www.gnu.org/licenses/>.

import src.anchor as a
import logging

from os.path import isfile

logging.basicConfig(level=logging.INFO)

BOLD = "\033[1m"
RESET = "\033[0m"
FAINT = "\033[2m"


class InteractiveResolver:
    def __init__(self, ANCHORS, version_provider, versions):
        self.ANCHORS = ANCHORS
        self.current_anchor = None
        self.current_candidates = None
        self.results = {}
        self.version_provider = version_provider
        self.versions = versions

        self.ACTIONS_HANDLER = {
            "c": self.choose,
            "q": self.quit,
            "h": self.help,
            "n": self.new,
            "d": self.delete,
            "i": self.info,
        }

    def loop(self, weighted_candidates_anchors):
        """ Loop the interaction for all the candidates. """
        for old_anchor, candidates in weighted_candidates_anchors.items():
            self.current_anchor = old_anchor
            self.current_candidates = candidates
            self.ask_anchor()
        self.end()
        return self.results

    def ask_anchor(self):
        "Ask the user if anchor is at the right place"
        self.display_anchor_context()
        if self.current_candidates:
            self.display_candidates()
        self.ask_user_action()

    def display_anchor_context(self):
        """ Display short context for the new set of candidates. """
        a = self.current_anchor
        if self.current_candidates:
            print(f"Anchor {a.anchor_id} in file {a.filename} has many candidates")
            print(
                "{:^4} | {:^4} | {:^10} | {:^15}".format(
                    "num", "id", "confidence", "lines"
                )
            )
            print("-" * 40)
        else:
            print(
                f"Anchor {a.anchor_id} in file {a.filename} does not have any candidate"
            )

    def display_candidates(self):
        """ Display each candidates in shot form. """
        for candidate_number, (anchor, confidence) in enumerate(
            self.current_candidates
        ):
            print(
                "{:^4} | {:^4} | {:^10} | {:>3}:{:<3}-{:>3}:{:<3}".format(
                    candidate_number,
                    anchor.anchor_id,
                    round(confidence, 2),
                    *anchor.lines,
                    *anchor.line_offsets,
                )
            )

    def display_anchor_context_verbose(self):
        """ Display the old anchor context in verbose mode. """
        anchor = self.current_anchor
        print("======= Anchor was BEFORE =======")
        print(f"------- In {anchor.filename} -------")
        labeled_anchor = {anchor: anchor.anchor_id}
        print(FAINT, end="")
        self.ANCHORS.cat_anchors(
            labeled_anchor, anchor.filename, self.version_provider, self.versions[0]
        )
        print(RESET, end="")

    def display_candidates_verbose(self):
        """ Display each candidat side by side with file content.
        It's verbose mode. """

        # Sort candidates by files
        candidates_by_file = {}

        for anchor, coeff in self.current_candidates:
            if anchor.filename not in candidates_by_file:
                # Create new anchor sublist
                candidates_by_file[anchor.filename] = [(anchor, coeff)]
            else:
                # Add to the correct one
                candidates_by_file[anchor.filename] += [(anchor, coeff)]

        print("======= Candidates are =======")
        for filename, candidates in candidates_by_file.items():
            print(f"------- In {filename} -------")
            labeled_anchor = {
                anchor: "{}({}%)".format(candidate_number, int(coef * 100))
                for candidate_number, (anchor, coef) in enumerate(candidates)
            }
            print(FAINT, end="")
            self.ANCHORS.cat_anchors(
                labeled_anchor, anchor.filename, self.version_provider, self.versions[1]
            )
            print(RESET, end="")

    def ask_user_action(self):
        """ Default message for asking the user for an action. """
        while True:
            action = input(f"{BOLD}What to you want to do ? (h for help)\n>{RESET} ")
            if self.handle_action(action):
                break

    def ask_filename(self):
        """ Ask the user for a filename, provide the current anchor as default
        filename. """
        default = self.current_anchor.filename
        filename = input(f"{BOLD}Filename ('{default}'):{RESET} ")
        while not isfile(filename):
            if not filename:
                return default
            if filename == "q":
                return None
            filename = input(f"{BOLD}Filename ('{default}'):{RESET} ")
        return filename

    def ask_line(self, filename, start=True):
        """ Ask the user for a line and an offset. Display vary if the line is
        the first or the last line of the anchor. """
        with open(filename, "r") as f:
            content = f.readlines()

            if start:
                print("Lets choose the first point <line:offset>")
            else:
                print("Lets choose the last point <line:offset>")

            # LINE NUMBER
            line_number = ""
            while not line_number.isdigit() or not (
                0 <= int(line_number) < len(content)
            ):
                print(FAINT, end="")
                for i, line in enumerate(content):
                    print(f"{i:3} {line.strip()}")
                print(RESET, end="")
                line_number = input(f"{BOLD}Line number ?{RESET} ")
                if line_number == "q":
                    return None, None

            # OFFSET
            offset = ""
            line = content[int(line_number)]
            while not offset.isdigit() or not (0 <= int(offset) <= len(line)):
                for i in range(0, len(line), 5):
                    print(f"{i:<5}", end="")
                print()
                for i in range(0, len(line), 5):
                    print(f"{'|':<5}", end="")
                print()
                for i in range(0, len(line), 5):
                    print(BOLD + line[i] + RESET + line[i + 1 : i + 5], end="")
                print()
                offset = input(f"{BOLD}Offset ?{RESET} ")
                if offset == "q":
                    return None, None

            return int(line_number), int(offset)

    def handle_action(self, action):
        """ Handle the command of the user using the `self.ACTIONS_HANDLER`
        dictionnary. """
        elements = action.split(" ")
        action = elements[0]
        args = elements[1:]
        if action in self.ACTIONS_HANDLER:
            return self.ACTIONS_HANDLER[action](args)
        else:
            print(f'"{action}" : action not recognized')

    def end(self):
        """ Called when the interactive resolution is finished. """
        pass
        # logging.info(
        #     "Finished ! The results that would be saved are\n %s", self.results
        # )

    #### ACTIONS ####

    def choose(self, args):
        """ Select the anchor as chosen candidate. """
        if args:
            try:
                candidate_number = int(args[0])
                if not (0 <= candidate_number < len(self.current_candidates)):
                    candidate_number = self.ask_choose(candidate_number)
            except ValueError as e:
                print(f'"{args[0]}" is not a valid candidate number. {e}')
                candidate_number = self.ask_choose(args[0])
        else:
            candidate_number = self.ask_choose("")
        if candidate_number or candidate_number == 0:
            self.results[self.current_anchor] = self.current_candidates[
                candidate_number
            ]
            print(f'Candidate "{candidate_number}" has been chosen')
            return True  # Action is finished

    def ask_choose(self, candidate=None):
        """ Ask the user for a candidate id. """
        candidate_number = candidate
        while not isinstance(candidate_number, int) or not (
            0 <= candidate_number < len(self.current_candidates)
        ):
            if candidate_number:
                print(f'"{candidate_number}" is not a valid candidate number.')
            try:
                input_str = input("Which candidate do you want ?\n-> ")
                if input_str == "q":
                    return None
                candidate_number = int(input_str)
            except Exception as e:
                print(f'"{input_str}" is not a valid interger. {e}')
                candidate_number = None
        return candidate_number

    def quit(self, args):
        """ Quit the program, nothing is saved. """
        print("Quitting ! New anchors will not be saved !")
        exit(0)

    def info(self, args):
        """ Display the old anchor and the candidates in verbose mode. """
        self.display_anchor_context_verbose()
        if self.current_candidates:
            self.display_candidates_verbose()

    def new(self, args):
        """ Add a new candidate manually. """
        filename = self.ask_filename()
        if not filename:
            return False

        start, start_offset = self.ask_line(filename, start=True)
        print(start, start_offset)
        if start is None or start_offset is None:
            return False

        end, end_offset = self.ask_line(filename, start=False)
        if end is None or end_offset is None:
            return False

        new_anchor = a.Anchor(0, filename, (start, end), (start_offset, end_offset))
        self.current_candidates.append((new_anchor, 1))

    def help(self, args):
        """ Show help. """
        for key, function in self.ACTIONS_HANDLER.items():
            print("{} : {}".format(key, function.__name__))

    def delete(self, args):
        """ Choose to not select a new candidate for the current old anchor. """
        self.results[self.current_anchor] = None
        print("Anchor is deleted")
        return True
