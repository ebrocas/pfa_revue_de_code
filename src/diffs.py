# Copyright (C) 2019 Eloïse Brocas, Mert Ernez, Jérôme Faure, Raphaël Gilliot
# Lucas Henry, Yann Julienne, Benjamin Le Rohellec
#
# This file is part of coderev, a code review tool.
#
# coderev is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# coderev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with coderev.  If not, see <https://www.gnu.org/licenses/>.
"""
This module provide the possibility to compute diffs between two files.
The diffs are created using the `unidiff` python module, and are bufferised to
reduce computation.
"""
from difflib import unified_diff

from unidiff import PatchSet

from .versionning import VersionProvider


class Diff:
    """ Base class for computing diffs. """

    def __init__(
        self, vprovider: VersionProvider, old_hash: str, new_hash: str, file: str
    ):
        self.vprovider = vprovider
        self.old_hash = old_hash
        self.new_hash = new_hash
        self.filename = file

    def compute(self) -> None:
        """ Analyse the file.
        This action is not performed during the init because the object could be
        destroyed before use.
        """
        raise NotImplementedError()

    def __eq__(self, other):
        return (
            isinstance(other, self.__class__)
            and self.old_hash == other.old_hash
            and self.new_hash == other.new_hash
            and self.filename == other.filename
        )


class DiffLine(Diff):
    """ Read the old and the new version of a file and compute the standard diff.
    The result (hunks) is stored on the patch_set variable.
    """

    def __init__(
        self, vprovider: VersionProvider, old_hash: str, new_hash: str, file: str
    ):
        super(DiffLine, self).__init__(vprovider, old_hash, new_hash, file)
        self.diff = None
        self.patch_set = None

    def compute(self):
        with self.vprovider.file_content(
            self.old_hash, self.filename
        ) as old, self.vprovider.file_content(self.new_hash, self.filename) as new:
            self.diff = unified_diff(
                old.readlines(), new.readlines(), self.filename, self.filename
            )
            self.patch_set = PatchSet("".join(self.diff))


class DiffBuffer:
    """ Register all diffs asked by the sub-strategies to reduce computations.
    """

    def __init__(self):
        self._diffs_line = list()

    def get_diff_line(
        self, vprovider: VersionProvider, old_hash: str, new_hash: str, filename: str
    ) -> DiffLine:

        for diff in self._diffs_line:
            if (
                diff.old_hash == old_hash
                and diff.new_hash == new_hash
                and diff.filename == filename
            ):
                return diff

        diff = DiffLine(vprovider, old_hash, new_hash, filename)
        diff.compute()
        self._diffs_line.append(diff)
        return diff

    def clear(self):
        self._diffs_line.clear()


diffbuffer = DiffBuffer()
get_diff_line = diffbuffer.get_diff_line
