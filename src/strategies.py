# Copyright (C) 2019 Eloïse Brocas, Mert Ernez, Jérôme Faure, Raphaël Gilliot
# Lucas Henry, Yann Julienne, Benjamin Le Rohellec
#
# This file is part of coderev, a code review tool.
#
# coderev is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# coderev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with coderev.  If not, see <https://www.gnu.org/licenses/>.

"""
List of strategies to be applied to anchors in order to updates its data.
"""

import difflib as d
import logging
from typing import List, Tuple, Iterable
import unidiff

from .anchor import Anchor, AnchorState
from .diffs import diffbuffer
from .globals import levenstein_strategy
from .versionning import VersionProvider


def inside(line, hunk) -> bool:
    """Return whether `line` is inside `hunk`
    :rtype: bool
    """
    return (int(line) >= hunk.source_start) and (
        int(line) < (hunk.source_start + hunk.source_length)
    )


def touch(anchor, hunk) -> bool:
    """ Return whether `hunk` is concerned by `anchor`. """
    return inside(anchor.lines[0], hunk) or inside(anchor.lines[1], hunk)


class Strategy:
    """
    Base Strategy class. This class does nothing and is not intented to be used
    directly. All strategies should derive from this class.
    """

    def __init__(self, vprovider: VersionProvider):
        self._vprovider = vprovider

    def _copy_anchor(self, versions: Tuple[str, str], anchor: Anchor) -> Anchor:
        """
        Create a copy of an anchor and change the filename to the new and
        correct filename, used in the last version.
        """
        a = anchor.__copy__()
        a.filename = self._vprovider.rename(versions[0], versions[1], anchor.filename)
        return a

    def apply(
        self, versions: Tuple[str, str], anchors: List[Anchor]
    ) -> Iterable[Tuple[int, Anchor, float]]:
        """
        The apply method use its logic to suggest a new position for each anchor
        of the entry.

        The new position are returned in the form of a new Anchor, <b>not
        registered in the AnchorManager</b>, and a number between 0 and 100 (%)
        to specify how sure is the position. The strategy must change the value

        A pure anchor is returned instead of some number to respect the format
        of the original anchor, no matter what kind of information is saved
        inside. Moreover, a maximum of anchor is treated at the same time to
        increase efficiency, like global moving in the file.

        :param versions: identifiers of the old versions which provide anchors,
        and the new versions
        :param anchors: list of Anchors to update, all the anchors come from the
        same file
        :return: a list of (the old anchor id, the new anchor and a number
        between 0 and 100 inclusive)
        """
        raise NotImplementedError("Function apply not ready")


class BasicDiffStrategy(Strategy):
    """
    Base class for strategies that use the classic diff system.
    This class does nothing and is not intented to be used directly.
    All strategies using the classic diff should derive from this class.
    """

    def __init__(self, vprovider: VersionProvider):
        super(BasicDiffStrategy, self).__init__(vprovider)

    def apply(
        self, versions: Tuple[str, str], anchors: List[Anchor]
    ) -> Iterable[Tuple[Anchor, Anchor, float]]:
        """
        Default apply method. Return a list of new anchors that are copies
        of anchors in ANCHORS.
        """
        created_anchors: List[Tuple[Anchor, Anchor, float]] = list()
        if len(anchors) == 0:
            return created_anchors
        filename = anchors[0].filename

        diff = diffbuffer.get_diff_line(
            self._vprovider, versions[0], versions[1], filename
        )
        patch_set = diff.patch_set  # List of all changes

        for patch in patch_set:
            # Computing anchors concerned by the patch
            logging.debug("BasicDiffStrategy: Working on %d anchors", len(anchors))

            for hunk in patch:
                logging.debug("BasicDiffStrategy: hunk %s", hunk)
                for anchor in anchors:
                    anchor_created = self.apply_hunk(anchor, hunk)

                    if anchor_created:
                        new_anchor, coef = anchor_created
                        created_anchors.append(
                            (anchor, self._copy_anchor(versions, new_anchor), coef)
                        )

        return created_anchors

    def apply_hunk(self, anchor: Anchor, hunk: unidiff.Hunk) -> Anchor:
        """
        Apply the modification of the `hunk` to the `anchor`.
        Return the new anchor if modified, else None.
        :rtype: Anchor, Int
        """
        raise NotImplementedError()


class SimpleStrategy(BasicDiffStrategy):
    """
    Simplest strategy. It works on anchor with only one line.
    If the modification is before the anchor, update the line.
    Otherwise, do nothing.
    """

    def apply_hunk(self, anchor: Anchor, hunk: unidiff.Hunk):
        modified = False
        new_lines: List[int] = list(anchor.lines)

        for i in range(len(new_lines)):

            if int(new_lines[i]) > hunk.source_length + hunk.source_start:
                # In this case, the hunk is before the anchor, we need to modify
                # the anchor
                logging.debug("%s: Hunk is before anchor: %s", __name__, anchor)
                line_diff = hunk.target_length - hunk.source_length
                new_lines[i] += line_diff
                modified = True

            if inside(new_lines[i], hunk):
                logging.debug("Hunk contains anchor: %s", anchor)
                src_line = [l for l in hunk if l.source_line_no == new_lines[i]][0]

                # line is not changed, but line is modified
                if src_line.target_line_no != new_lines[i]:
                    new_lines[i] = src_line.target_line_no
                    modified = True

        if modified:
            if new_lines[0] is not None and new_lines[1] is not None:
                new_anchor = Anchor(
                    0,
                    anchor.filename,
                    lines=(new_lines[0], new_lines[1]),
                    offsets=anchor.line_offsets,
                )
                new_anchor.set_state(AnchorState.CONFIRMED)
                return new_anchor, 1.0
            else:
                new_anchor = anchor.__copy__()
                new_anchor.set_state(AnchorState.ERROR)
                return new_anchor, 0.0
        else:
            return anchor, 1.0


class LevensteinStrategy(BasicDiffStrategy):
    """
    Levenstein strategy. Update
    If the modification is before the anchor, update the line.
    Otherwise, do nothing.
    """

    def __init__(self, vprovider: VersionProvider, minimum_ratio=levenstein_strategy):
        super(LevensteinStrategy, self).__init__(vprovider)
        self.min_ratio = minimum_ratio

    def apply_hunk(self, anchor: Anchor, hunk: unidiff.Hunk):
        new_lines = []
        ratios = []
        for anchor_line in anchor.lines:
            if inside(anchor_line, hunk):
                # line has been modified
                src_line = [l for l in hunk if l.source_line_no == int(anchor_line)][0]
                added_lines = [l for l in hunk if (l.is_added or l.is_context)]
                line, ratio = LevensteinStrategy.best_match(src_line, added_lines)
                if line:
                    new_lines.append(line.target_line_no)
                    ratios.append(ratio)
            else:
                new_lines.append(anchor_line)

        if list(anchor.lines) != new_lines:
            new_anchor = Anchor(
                0, anchor.filename, new_lines, anchor.line_offsets, anchor.data
            )
            return (new_anchor, sum(ratios) / 2)
        return None

    @staticmethod
    def best_match(src_line, added_lines):
        """
        Return the best line that match `src_line`
        """
        best_ratio = 0
        best_line = None
        for added_line in added_lines:
            ratio = d.SequenceMatcher(None, src_line.value, added_line.value).ratio()
            if ratio > best_ratio:
                best_ratio = ratio
                best_line = added_line
        return best_line, best_ratio


class GlobalLevensteinStrategy:
    """
    Global Levenstein strategy. Useful to detect deplacement of lines. """

    def __init__(self, vprovider: VersionProvider, minimum_ratio=levenstein_strategy):
        self.min_ratio = minimum_ratio
        self._vprovider = vprovider

    def apply(
        self, versions: Tuple[str, str], anchors: List[Anchor]
    ) -> Iterable[Tuple[Anchor, Anchor, float]]:
        before, after = versions
        candidates = []
        for anchor in anchors:
            before_file = list(self._vprovider.file_content(before, anchor.filename))
            after_file = list(self._vprovider.file_content(after, anchor.filename))
            lines = []
            ratios = []
            for line in anchor.lines:
                before_line = before_file[line]
                line_number, after_line, ratio = GlobalLevensteinStrategy.best_match(
                    before_line, after_file
                )
                ratios.append(ratio)
                lines.append(line_number)
            candidates.append(
                (
                    anchor,
                    Anchor(0, anchor.filename, lines, anchor.line_offsets),
                    sum(ratios) / 2,
                )
            )
        return candidates

    @staticmethod
    def best_match(src_line, candidat_lines):
        """
        Return the best line that match `src_line`
        """
        best_ratio = 0
        best_line = None
        best_line_number = 0
        for i, candidat_line in enumerate(candidat_lines):
            ratio = d.SequenceMatcher(None, src_line, candidat_line).ratio()
            if ratio > best_ratio:
                best_ratio = ratio
                best_line = candidat_line
                best_line_number = i
        return best_line_number, best_line, best_ratio


class KeepEverythingStrategy(Strategy):
    """
    This strategy propose the old anchors as candidates.
    Usefull when files are not modified.
    """

    def __init__(self, vprovider):
        super().__init__(vprovider)
        self.vprovider = vprovider

    def apply(self, versions, anchors):
        candidates = []
        for anchor in anchors:
            if not KeepEverythingStrategy.file_changed(
                anchor.filename, versions, self.vprovider
            ):
                candidates.append((anchor, anchor, 1))
            else:
                candidates.append((anchor, anchor, 0.1))
        return candidates

    @staticmethod
    def file_changed(filename, versions, vprovider):
        before_file = list(vprovider.file_content(versions[0], filename))
        after_file = list(vprovider.file_content(versions[1], filename))
        return all([a == b for a, b in zip(before_file, after_file)])
