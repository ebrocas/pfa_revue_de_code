# Copyright (C) 2019 Eloïse Brocas, Mert Ernez, Jérôme Faure, Raphaël Gilliot
# Lucas Henry, Yann Julienne, Benjamin Le Rohellec
#
# This file is part of coderev, a code review tool.
#
# coderev is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# coderev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with coderev.  If not, see <https://www.gnu.org/licenses/>.

import logging
from .anchor import AnchorManager
from .strategies import (
    SimpleStrategy,
    LevensteinStrategy,
    KeepEverythingStrategy,
    GlobalLevensteinStrategy,
)
from .interactive_resolver import InteractiveResolver
from .conversion import save_anchors

STRATEGIES_MAP = {
    "simple": SimpleStrategy,
    "leven": LevensteinStrategy,
    "keep": KeepEverythingStrategy,
    "gleven": GlobalLevensteinStrategy,
}
DEFAULT_STRATEGIES = ["simple", "leven", "keep", "gleven"]


class AnchorUpdater:
    """
    This class is responsible of updating a maximum of anchor automatically.
    All the strategies selected by the client will be used, if needed to
    evaluate a new position for the anchor.

    The update take in entry a list of anchors to update and return a map giving
    the news anchors associated with the old anchors. The new anchors can be
    marked CONFIRMED if the algorithm is sure of the new position, AMBIGUOUS if
    not. In case the updater didn't find a new position for the anchor, a new
    anchor is returned tagged ERROR, located to the last known good position.
    """

    def __init__(
        self,
        anchor_manager,
        version_provider,
        strategies=DEFAULT_STRATEGIES,
        should_save=True,
    ):
        if not strategies:
            strategies = DEFAULT_STRATEGIES
        self.strats = [STRATEGIES_MAP[strat_name] for strat_name in strategies]
        self.anchor_manager = anchor_manager
        self.should_save = should_save
        self.version_provider = version_provider

    def update(self, versions, auto=True):
        """
        Ask the strategies for a new place for each anchors in the list and
        update them.
        :param versions: (old_hash, new_hash) identifiers of the versions
        :return: None
        """
        candidates = self.apply_strategies(versions)

        candidates = self.apply_meta_strategy(candidates)

        if auto:
            mono_candidates = self.resolve_automatically(candidates)
        else:
            mono_candidates = self.resolve_interactively(candidates, versions)

        if self.should_save:
            self.save(mono_candidates, versions)

        return mono_candidates

    def apply_strategies(self, versions):
        """ Apply strategies between the two versions. Returns candidates
        grouped by old anchor. """
        candidates = []
        anchors = self.anchor_manager.anchors
        # Applying strategies
        for strat in self.strats:
            candidates += strat(self.version_provider).apply(versions, anchors)

        # Reorganizing candidates in a dictionnary
        # We group candidates to a same anchor
        organized_candidates = dict()
        for a in anchors:
            organized_candidates[a] = []
        # organized_candidates = {a: [] for a in anchors}

        for old_anchor, new_anchor, confidence in candidates:
            organized_candidates[old_anchor].append((new_anchor, confidence))

        return organized_candidates

    def apply_meta_strategy(self, candidates):
        """ Apply the meta strategy. Candidates are ponderated in order to
        mutualize informations. """

        weighted_candidates_association = {}

        # Applying ponderation to candidates
        for anchor, candidates in candidates.items():
            weighted_candidates = AnchorUpdater.ponderate(candidates)
            weighted_candidates_association[anchor] = weighted_candidates

        return weighted_candidates_association

    def resolve_interactively(self, candidates, versions):
        """ Use the interactive resolution method. """
        resolver = InteractiveResolver(
            self.anchor_manager, self.version_provider, versions
        )
        return resolver.loop(candidates)

    def resolve_automatically(self, candidates):
        """ Use the automatic resolutino method. """
        result = {}
        for anchor, new_anchors_candidates in candidates.items():
            best_candidat = None
            best_coef = 0
            for candidat, coef in new_anchors_candidates:
                if coef >= best_coef:
                    best_candidat = candidat
                    coef = best_coef
            result[anchor] = best_candidat
        return result

    def save(self, mono_candidates, versions):
        """ Save the candidates into the new version, keep the anchors ids. """
        a = AnchorManager()
        for anchor, coef in mono_candidates.values():
            a.add_anchor(anchor, keep_id=True)
        print(a.anchors)
        save_anchors(a, versions[1])

    @staticmethod
    def ponderate(candidates):
        """ Duplicates are removed and the sum of all coefficient is shared
        between the candidates."""
        weighted_candidates = {}

        # Update coefs using around anchors
        for base_anchor, base_coeff in candidates:
            weighted_candidates[base_anchor] = 0
            for anchor, coeff in candidates:
                weighted_candidates[base_anchor] += coeff / (
                    1 + distance(anchor, base_anchor)
                )

        # Remove dups
        uniq_candidates = {}
        for anchor, coeff in weighted_candidates.items():
            if not any(
                [
                    anchor.lines == a.lines and anchor.line_offsets == a.line_offsets
                    for a in uniq_candidates.keys()
                ]
            ):
                uniq_candidates[anchor] = coeff

        # Normalisation
        sum_coef = sum(uniq_candidates.values())

        if sum_coef != 0:
            return [(a, uniq_candidates[a] / sum_coef) for a in uniq_candidates]
        return [(a, coef) for a, coef in uniq_candidates.items()]


def distance(anchor1, anchor2):
    """ Return a number between 0 and +inf caracterizing if anchor1 and anchor2
    are near each other.
    """

    f1, l1 = anchor1.lines
    fo1, lo1 = anchor1.line_offsets

    f2, l2 = anchor2.lines
    fo2, lo2 = anchor2.line_offsets

    lines_diff = abs(f1 - f2) + abs(l1 - l2)
    offsets_diff = abs(fo1 - fo2) + abs(lo1 - lo2)

    # Values are arbitrary
    return 0.5 * lines_diff + 0.1 * offsets_diff
