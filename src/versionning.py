# coding:utf-8
import subprocess
from io import TextIOWrapper
from os import getcwd
from os.path import split, exists, join, relpath, abspath
from typing import Optional, TextIO


class VersionProvider:
    """
    This tool can give the whole content of a file in a specific version.
    It required that a versionning system was present in the working directory
    or two directory to use as the old version and the new version.
    """

    def actual_version(self) -> str:
        """ Give the last known commit. """
        raise NotImplementedError()

    def previous_version(self) -> Optional[str]:
        """ If actual_version give the last commit, previous_version give the
        commit just before. So, after commiting your project, you can update
        from previous_version to actual_version.
        """
        raise NotImplementedError()

    def rename(self, version_old: str, version_new: str, filename: str):
        """ If a file has been renamed between the versions old and new, and the
        versionning system permit it, find the new name of a file which was
        present in the old version. """
        return filename

    def relpath(self, filename: str) -> str:
        """ Give the relative path of a file, from the root of the current
        project. If the program is not use in the root directory, paths should
        not be constant. """
        return filename

    def file_content(self, version: str, filename: str) -> TextIO:
        """ Return the whole content of a file for a specific version. The
        result is a text stream (decoded using utf8), the stream must be closed
        after use.
        :return: a stream where you can read the file
        """
        raise NotImplementedError()


class FileVersionProvider(VersionProvider):
    """ This version provider allow you to have an older version in one
    directory and the newer version in an other one. You need two path: the root
    of the older project's directory and the root of the newer. Only two
    versions are allowed "before" and "after", don't try to use something else.
    """

    def __init__(self, before_dir: str, after_dir: str):
        self._before = abspath(before_dir)
        self._after = abspath(after_dir)

    def actual_version(self) -> str:
        return "after"

    def previous_version(self) -> str:
        return "before"

    def rename(self, version_old: str, version_new: str, filename: str):
        return join(self._after, self.relpath(filename))

    def relpath(self, filename: str) -> str:
        """ The filename must be relative to root of the project's directory, be
        relative to the current working dir, or be absolute. The aim is to
        relativize the path the the project's root directory. """
        f = abspath(
            filename
        )  # DO NOT DELETE this line, or the next 4 will not work anymore
        if f.startswith(self._before):
            return relpath(f, self._before)
        elif f.startswith(self._after):
            return relpath(f, self._after)
        return super(FileVersionProvider, self).relpath(filename)

    def file_content(self, version: str, filename: str) -> TextIO:
        filename = self.relpath(filename)
        if version == "before":
            return open(join(self._before, filename), "r", encoding="utf8")
        elif version == "after":
            return open(join(self._after, filename), "r", encoding="utf8")
        else:
            raise KeyError("Version '{:s}' inconnue !".format(version))


class GitVersionProvider(VersionProvider):
    """
    Interface to read version information from a git repository.
    The actual version, will be the sha of HEAD. It's more the last commited
    version.

    Git doesn't support file's renaming because it works on content not files.
    """

    def __init__(self, working_dir=getcwd()):
        self._head = None
        self.repo = None

        while working_dir:
            if exists(join(working_dir, ".git")):
                self.repo = working_dir
            working_dir, tail = split(working_dir)
            if not tail:
                break

    def rename(self, version_old: str, version_new: str, filename: str):
        if self.repo:
            join(self.repo, self.relpath(filename))
        return super(GitVersionProvider, self).rename(
            version_old, version_new, filename
        )

    def relpath(self, filename: str) -> str:
        f = abspath(filename)
        if self.repo and f.startswith(self.repo):
            return relpath(f, self.repo)
        return super(GitVersionProvider, self).relpath(filename)

    def actual_version(self) -> Optional[str]:
        if self._head is None:
            result = subprocess.run(
                ["git", "rev-parse", "HEAD"], stdout=subprocess.PIPE, encoding="utf-8"
            )
            if result.returncode == 0:
                self._head = result.stdout[:-1]
        return self._head

    def previous_version(self) -> Optional[str]:
        result = subprocess.run(
            ["git", "rev-parse", "HEAD~1"], stdout=subprocess.PIPE, encoding="utf-8"
        )
        if result.returncode == 0:
            return result.stdout[:-1]
        return None

    def file_content(self, version: str, filename: str) -> TextIO:
        result = subprocess.Popen(
            ["git", "show", "{:s}:{:s}".format(version, filename)],
            stdout=subprocess.PIPE,
        )
        return TextIOWrapper(result.stdout, encoding="utf8")


def detect_version_system() -> Optional[VersionProvider]:
    """ Detect a versionning manager.
    Initialise the corresponding interface.
    """
    p = getcwd()
    while p:
        if exists(join(p, ".git")):
            return GitVersionProvider()
        p, tail = split(p)
        if not tail:
            break
    return None


def parse_ref(version_manager: VersionProvider, code: str) -> str:
    if code is None or code.lower() == "head":
        return version_manager.actual_version()
    if code.lower() == "head~1":
        return version_manager.previous_version()
    return code
