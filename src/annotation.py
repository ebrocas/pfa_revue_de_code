# Copyright (C) 2019 Eloïse Brocas, Mert Ernez, Jérôme Faure, Raphaël Gilliot
# Lucas Henry, Yann Julienne, Benjamin Le Rohellec
#
# This file is part of coderev, a code review tool.
#
# coderev is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# coderev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with coderev.  If not, see <https://www.gnu.org/licenses/>.

import logging
from typing import *


class Reference(Sized):
    def __init__(self, anchors: List[int] = None):
        self._anchors: List[int] = anchors if anchors is not None else list()

    def addAnchor(self, anchor: int) -> None:
        """ add a new anchor to the Reference """
        self._anchors.append(anchor)

    def files(self, ANCHORS):
        """
        Files in which the reference is present.
        :return: a set of filenames containing the anchors
        """
        files = set()
        for anchor_id in self._anchors:
            anchor = ANCHORS.get_by_id(anchor_id)
            if anchor:
                files.add(anchor.filename)
            else:
                print("ANCHOR_ID: ", anchor_id)
                logging.info("Reference:files:anchor %d not found", anchor_id)
        return files

    def lines_in_file(self, ANCHORS, filename: str):
        """
        Lines concerned by the reference in `filename`.
        :return: sorted list of lines
        """
        lines = []
        for anchor_id in self._anchors:
            anchor = ANCHORS.get_by_id(anchor_id)
            if anchor.filename == filename:
                lines.append(anchor.lines)
        lines.sort()
        return lines

    def __contains__(self, item):
        if isinstance(item, int):
            return item in self._anchors

    def __len__(self) -> int:
        return len(self._anchors)

    def __repr__(self):
        ret = list()
        for anchor in self._anchors:
            ret.append(repr(anchor))
        return repr(ret)

    def __str__(self):
        return repr(self)


class Annotation(Hashable):
    """ Coderev object which contains the data that the user want to add to a
    bloc of text.
    """

    _id_increment = 1

    def __init__(
        self, id: Optional[int], reference: Reference, title="", description="", tags=[]
    ):
        """
        :param id: Annotation identifier. Generated if setted on None.
        """
        assert id is None or isinstance(id, int)
        assert isinstance(reference, Reference)

        self.id = Annotation._id_increment if id is None else id
        Annotation._id_increment = max(Annotation._id_increment, self.id + 1)
        self.tags = tags if tags else []
        self.reference = reference
        self.title: str = title if title else ""
        self.description: str = description if description else ""
        self.open: bool = True

    def files(self, ANCHORS):
        """
        Files in which the annotation is present.

        :param ANCHORS: The AnchorManager
        """
        return self.reference.files(ANCHORS)

    def lines_in_file(self, ANCHORS, filename: str):
        """
        Lines concerned by the annotation in `filename`.
        :return: list of lines
        """
        return self.reference.lines_in_file(ANCHORS, filename)

    def close(self):
        """
        Set annotation status to closed state.
        """
        self.open = False

    def edit(
        self,
        anchors: List[int] = None,
        title: str = None,
        description: str = None,
        tags: List[str] = None,
    ):
        """ Edit the annotation with informations provided. For each argument,
        if it is not None, it will replace the current annotation value. """
        if anchors:
            self.reference._anchors = anchors
        if title:
            print("MODIFYING TITLE")
            self.title = title
        if description:
            self.description = description
        if tags:
            self.tags = tags

    def addTag(self, tag: str) -> None:
        """
        Add a tag to the annotation tag list.
        """
        self.tags.add(tag)

    def removeTag(self, tag: str) -> None:
        """
        Remove a tag fro the annotation tag list.
        """
        self.tags.remove(tag)

    def set_title(self, title: str):
        """
        Set annotation title.
        """
        self.title = title

    def set_description(self, description: str):
        """
        Set annotation description.
        """
        self.description = description

    def __repr__(self) -> str:
        return (
            "Annotation #"
            + str(self.id)
            + " <"
            + ", ".join(self.tags)
            + ">"
            + repr(self.reference)
        )

    def __hash__(self):
        return self.id


class AnnotationManager:
    """
    This manager register all knows annotations.
    It is the main interface to access and modify annotations. It's also responsible of saving and importing annotations.
    """

    def __init__(self):
        self._annotations: Dict[int, Annotation] = dict()

    def get(self, i: int) -> Annotation:
        """
        :return: the annotation associated with this id
        """
        return self._annotations.get(i)

    def __contains__(self, item) -> bool:
        return isinstance(item, int) and item in self._annotations.keys()

    def get_ids(self) -> Iterable[int]:
        """:return: the list of all knows annotation's ids."""
        return self._annotations.keys()

    def add(self, title: str, tags: List[str], description: str, anchors: List[int]):
        """
        Add a new annotation.
        :param anchors: a list of id of valid anchors.
        """
        # Building the reference
        ref = Reference()
        for a in anchors:
            ref.addAnchor(a)

        identifier = len(self._annotations) + 1
        ann = Annotation(identifier, ref, title, description, tags)
        self._annotations[identifier] = ann

    def annotation_in_file(self, ANCHORS, filename=None):
        """
        Filter anchors in ANCHORS parameter where, in the context of the manager, is contained in the file specified in filename parameter.
        :param ANCHORS: Anchors array to filter.
        :param filename: String representing a filename
        Return an array of anchors belong to file specified by filename if filename is provided.
        Return an array of all anchors known by the manager else.
        """
        if not filename:
            return self._annotations
        else:
            return [
                a for a in self._annotations.values() if filename in a.files(ANCHORS)
            ]

    def cat_annotations(
        self,
        ANCHORS,
        version_provider,
        version: str,
        filename: str = None,
        only_context=True,
    ):
        """
        Display annotations present in filename.
        :param version: version to load and display
        """
        if filename:
            anchors_in_file = [a for a in ANCHORS.anchors if a.filename == filename]
            labeled_anchors = {}
            for anchor in anchors_in_file:
                annotation = self.get_annotation_for_anchor(anchor.anchor_id)
                if annotation:
                    labeled_anchors[anchor] = annotation.title
                else:
                    labeled_anchors[anchor] = -1
            ANCHORS.cat_anchors(
                labeled_anchors, filename, version_provider, version, only_context
            )

    def get_annotation_for_anchor(self, anchor: int):
        """
        Return the annotation that has `anchor`.
        :param anchor: id of the anchor concerned.
        :return: the annotation linked with this anchor, or None
        """
        for annotation in self._annotations.values():
            if anchor in annotation.reference:
                return annotation
        return None

    def reference_modify(self, annotation_id: int, anchors: List[int]) -> None:
        """
        Change the list of anchors associated with an annotation in a specific version.
        :param annotation_id: the identifier of an annotation
        :param anchors: the list of all the anchors, they must be present in the same version, but this function doesn't
        check it
        """
        if annotation_id not in self._annotations:
            raise ValueError(
                "The annotation of id '{}' is unknown.".format(annotation_id)
            )
        else:
            annotation = self._annotations[annotation_id]
            annotation.reference = Reference(anchors)
