# Copyright (C) 2019 Eloïse Brocas, Mert Ernez, Jérôme Faure, Raphaël Gilliot
# Lucas Henry, Yann Julienne, Benjamin Le Rohellec
#
# This file is part of coderev, a code review tool.
#
# coderev is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# coderev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with coderev.  If not, see <https://www.gnu.org/licenses/>.

import pickle
from os import path, mkdir, makedirs, getcwd

from src import AnnotationManager, AnchorManager

totalPath = path.join(".data", "total.json")
DIR = ".coderev"
DIR_ANCHOR = path.join(DIR, "anchors")
FILE_ANNOTATION = path.join(DIR, "annotations.dat")


def root_dir() -> str:
    """ Detect the root dir of the project and the data directory """
    p = path.abspath(getcwd())
    while path.dirname(p) != p:
        if path.exists(path.join(p, DIR)):
            return p
        p = path.dirname(p)
    print("This directory is not located in a project.")
    exit(2)


def init_directory(dir: str):
    """ Create the `.coderev` directory. """
    directory = path.join(dir, DIR)
    if path.exists(directory):
        print("Coderev folder already existing.")
    else:
        mkdir(directory)
        print("Directory initialized.")


def save_all(anchors, annotations):
    """ Save all datas in a single files
    @deprecated
    """
    if not path.exists(".data"):
        mkdir(".data")
    with open(totalPath, "wb") as save_destination:
        pickle.dump((anchors, annotations), save_destination)


def load_all():
    """ Load the anchor manager and the annotation manager from the
    data file.  Expect that the data file is well formated.
    @deprecated
    """
    try:
        with open(totalPath, "rb") as saved_data:
            return pickle.load(saved_data)
    except Exception:
        return AnchorManager(), AnnotationManager()


def save_anchors(anchors: AnchorManager, version: str):
    """ Save the actual list of anchors.
    The anchor manager is saved and imported depending of the version
    needed. Assuming that the version parameter is the current
    version, save the anchor list in the corresponding file.
    """
    assert isinstance(anchors, AnchorManager)
    anchor = path.join(root_dir(), DIR_ANCHOR)
    if not path.exists(anchor):
        makedirs(anchor)

    with open(path.join(anchor, version), "wb") as file:
        pickle.dump(anchors, file)


def load_anchors(version: str) -> AnchorManager:
    """ Load the anchors present in a specific version.
    If the specified version, never existed, return an empty anchor manager.
    :return: a new AnchorManager containing all the anchors associated
    with this version.
    """
    anchor = path.join(root_dir(), DIR_ANCHOR)
    if path.exists(path.join(anchor, version)):
        with open(path.join(anchor, version), "rb") as file:
            manager = pickle.load(file)
            manager.post_import()
            return manager
    return AnchorManager()


def save_annotations(annotations: AnnotationManager):
    """ Save all the annotations.  The annotations exists through time
    and code evolution and don't require a version, but they can have
    no anchors present for an ancient or too new version."""
    annotation = path.join(root_dir(), FILE_ANNOTATION)
    with open(annotation, "wb") as file:
        pickle.dump(annotations, file)


def load_annotations() -> AnnotationManager:
    """ Load the annotations. If the annotation file is not found, return an
    empty AnnotationManager."""
    annotation = path.join(root_dir(), FILE_ANNOTATION)
    if path.exists(annotation):
        with open(annotation, "rb") as file:
            return pickle.load(file)
    return AnnotationManager()
