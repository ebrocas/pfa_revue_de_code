# coding:utf8
from .config import ConfigValue, ConfigValueFloat, Configuration

username = ConfigValue("user.name", "")
email = ConfigValue("user.email", "")

levenstein_strategy = ConfigValueFloat("strategy.levenstein.ratio", 0.8, min=0, max=1,
                                       comment="Two lines are considered too different if the similarity is above "
                                               "the ratio.")

CONFIG = Configuration("config.json")
CONFIG.register_value(username)
CONFIG.register_value(email)
CONFIG.register_value(levenstein_strategy)

CONFIG.load()
CONFIG.save()
