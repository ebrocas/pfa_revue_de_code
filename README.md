# coderev

While developping a project, many code parts can be quickly produced. In a collaborative environement, putting all those parts together in a coherent manner represents a significant work.
Developpers need to inspect the code produced by their co-workers in a process call code revue. The goal of this project is to simplify this task by providing tool allowing efficient discussion about the code.

### Requirements

Every requirement can be found in the file requirements.txt.

### Usage

Using coderev.py with python3, eight actions can be done :

- Create a new anchor : `coderev.py add --anchor FILE LINE`

  - FILE and LINE are used to locate the part of the code concerned by the comment
  - If present, can be use to create a new annotation.

- Create a new annotation : coderev.py add --annotation

  - with the following options :
  - `--title [TITLE]`, `-t [TITLE]` : Title for the annotation
  - `--tags [TAGS [TAGS ...]]` : List of tags for the annotation
  - `--description DESCRIPTION`, `-d DESCRIPTION` : Description for the annotation
  - `--ref [hash [hash ...]]` : identifiers of anchors to use. An anchor is required, use `--ref` or `--anchor` to provide an anchor

- Display annotations : coderev.py display annotation
  -with the following options :

  - `--all`/`-a` : Display all available, restricted by filters.
  - `--file`/`-f file [file ...]`: restrict to annotations present in a files.
  - `--line`/`-l line [line ...]` : restrict to annotations at a specified line.
  - `id` : Select an annotation by its id.

- Display anchors : coderev.py display anchors

  - with the same options than annotations

- Update all annotations after modifying the code in a file : coderev.py update old_file new_file

  - It is also possible to decide not modifying the anchors with the option -d or --dry-run
  - The `-s` option let the user specify the strategies that sould be applied.

- Display the file content and annotations together : coderev.py cat file

- Edit an annotation : python3 coderev.py edit annotation

  - with the following options :
  - `--id ID` : Id of the annotation
  - `--title TITLE` : Title for the annotation
  - `--tags TAGS [TAGS ...]` : List of tags for the annotation
  - `--description DESCRIPTION` : Description for the annotation
  - `--ref hash [hash ...]` : Identifiers of anchors to use.

- Edit an anchor : python3 coderev.py edit anchor
  - with the following options :
  - `--id ID` : Edit the anchor
  - `--file FILE` : File of the anchor
  - `--line LINE` : Line of the anchor

Those informations can also be found by using the optional argument -h or --help.

### Examples

An example workflow would be :

```sh
# create a first annotation
coderev add --annotation --title "First annotation" --anchor REAME.md 10 13
# Create an anchor, and then add it to the annotation
coderev add --anchor Makefile 5 2
coderev edit annotation 1 --ref 1 2

# Display annotations and anchors
coderev display annotation
coderev display annotation 1
coderev display anchor

# Displaying files
coderev cat README.md
coderev cat Makefile

# After a new commit :
coderev update
```

## Authors

- Eloïse Brocas
- Mert Ernez
- Jérôme Faure
- Raphaël Gilliot
- Lucas Henry
- Yann Julienne
- Benjamin Le Rohellec

## License

coderev is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or(at your option) any later version.

coderev is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with coderev. If not, see <https://www.gnu.org/licenses/>.
