# coding:utf8
import argparse
import os.path
from sys import stderr

from src import conversion
from src.versionning import VersionProvider, parse_ref


def create_subparser_add(parser):
    g_ref = parser.add_argument_group("references")
    g_ref.add_argument(
        "--anchor",
        nargs="+",
        metavar=("FILE", "LINE[:OFFSET]"),
        help="Create a new anchor. If present, can be use to create a new annotation.",
    )
    g_ref.add_argument(
        "--version",
        "-v",
        type=str,
        help="Select a different version from the current version",
    )

    g_ann = parser.add_argument_group("annotation")
    g_ann.add_argument(
        "--annotation", action="store_true", help="Create a new annotation"
    )
    g_ann.add_argument(
        "--title",
        "-t",
        nargs="?",
        dest="title",
        type=str,
        help="Title for the annotation",
    )
    g_ann.add_argument(
        "--tags",
        nargs="*",
        dest="tags",
        type=str,
        help="List of tags for the annotation",
    )
    g_ann.add_argument(
        "--description", "-d", type=str, help="Description for the annotation"
    )
    g_ann.add_argument(
        "--ref",
        nargs="*",
        metavar="hash",
        type=int,
        help="identifiers of anchors to use. An anchor is required, use --ref or --anchor to provide an anchor",
    )


def execution(arguments: argparse.Namespace, versionP: VersionProvider):

    # Assert file exists
    if arguments.anchor and not os.path.exists(arguments.anchor[0]):
        print("Unknow file: '" + arguments.anchor[0] + "'", file=stderr)

    # Loading anchors and annotations
    version_id = parse_ref(versionP, arguments.version)
    ANCHORS = conversion.load_anchors(version_id)

    anchors = []

    # Anchors from the --ref arg
    if arguments.ref:
        for anchor_id in arguments.ref:
            # check existence
            if ANCHORS.get_by_id(anchor_id):
                anchors.append(anchor_id)
            else:
                print("WARNING: anchor {} not found".format(anchor_id))

    # Anchors from the --anchor arg
    if arguments.anchor:
        # Creating anchor
        anchor = arguments.anchor
        filename = anchor[0]
        lines = [0, -1]
        offsets = [0, 0]

        if len(anchor) >= 2:
            pos1 = str(anchor[1])
            if ":" in pos1:
                s = pos1.index(":")
                lines[0] = lines[1] = int(pos1[:s])
                offsets[0] = offsets[1] = int(pos1[s + 1 :])
            else:
                lines[0] = lines[1] = int(pos1)
        else:
            raise ValueError("I need at least one line to place the anchor.")

        if len(anchor) >= 3:
            pos2 = anchor[2]
            if ":" in pos2:
                s = pos2.index(":")
                lines[1] = int(pos2[:s])
                offsets[1] = int(pos2[s + 1 :])
            else:
                lines[1] = int(pos2)

        anchor_id = ANCHORS.add(filename, lines, offsets)
        anchors.append(anchor_id)

    if arguments.annotation:
        ANNOTATIONS = conversion.load_annotations()
        ANNOTATIONS.add(arguments.title, arguments.tags, arguments.description, anchors)
        conversion.save_annotations(ANNOTATIONS)

    # Saving to disk
    conversion.save_anchors(ANCHORS, version_id)
