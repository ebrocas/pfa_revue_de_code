ORIGINAL FILE
1 int a = 1;
2
3 if ( a == 0){
4   return 0;
5 }else{
6   return 1;
7 }

ALTERED FILE
1 int a = 1;
2
3 if ( a == 0){return 0;}else{return 1;}



With the simple strategie we observe that :
Success rate : 0.42857142857142855 with strategies ['simple']
Anchor on line 1 was expected on 1 and was bumped on 1
Anchor on line 2 was expected on 2 and was bumped on 2
Anchor on line 3 was expected on 3 and was bumped on 3
Anchor on line 4 was expected on 3 and was bumped on 4
Anchor on line 5 was expected on 3 and was bumped on 5
Anchor on line 6 was expected on 3 and was bumped on 6
Anchor on line 7 was expected on 3 and was bumped on 7


With the leven strategie we observe that :



Other observation :
