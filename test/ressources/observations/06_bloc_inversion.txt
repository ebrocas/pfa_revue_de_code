ORIGINAL FILE
1 void foo(){
2   prinf("o");
3 }
4
5 void fii(){
6   prinf("i");
7 }
8
9 void faa(){
10   prinf("a");
11 }

ALTERED FILE
1 void faa(){
2   prinf("a");
3 }
4 void foo(){
5   prinf("o");
6 }
7 void fii(){
8   prinf("i");
9 }


With the simple strategie we observe that :
Success rate : 0.3333333333333333 with strategies ['simple']
Anchor on line 1 was expected on 4 and was bumped on 1
Anchor on line 2 was expected on 5 and was bumped on 2
Anchor on line 3 was expected on 6 and was bumped on 3
Anchor on line 4 was expected on 4 and was bumped on 4
Anchor on line 5 was expected on 5 and was bumped on 5
Anchor on line 6 was expected on 6 and was bumped on 6
Anchor on line 7 was expected on 1 and was bumped on 7
Anchor on line 8 was expected on 2 and was bumped on 8
Anchor on line 9 was expected on 3 and was bumped on 9


With the leven strategie we observe that :



Other observation :
