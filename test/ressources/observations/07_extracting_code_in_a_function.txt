ORIGINAL FILE
1 int a = 1;
2 int b = 2;
3
4 int main(){
5   int c = a + b;
6   printf("%d = %d + %d\n",c,a,b);
7   return 0;
8 }

ALTERED FILE
1 int a = 1;
2 int b = 2;


With the simple strategie we observe that :
Success rate : 0.375 with strategies ['simple']
Anchor on line 1 was expected on 1 and was bumped on 1
Anchor on line 2 was expected on 2 and was bumped on 2
Anchor on line 3 was expected on 3 and was bumped on 3
Anchor on line 4 was expected on 8 and was bumped on 4
Anchor on line 5 was expected on 9 and was bumped on 5
Anchor on line 6 was expected on 10 and was bumped on 6
Anchor on line 7 was expected on 11 and was bumped on 7
Anchor on line 8 was expected on 12 and was bumped on 8


With the leven strategie we observe that :


Other observation :
