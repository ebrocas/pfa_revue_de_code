This directory contains one file for each exemple case with the same format :
two columns with on the first column a number representing the position of a
line for the first version and on the second column a number representing the
expected position of the same line in the new file (after the use of the update
command).
