Repository organisation :
=========================

This repository contains exemples that can be used to test update strategies.

The repositories before and after provide the old and the new version of code
exemples.

The results expected can be found in the repository expectations and our
observations about the results are in the repository of the same name.

Every exemple file is identified by a number and a short description included in
its name.


Strategies tested :
==================

For now only the simple strategie is tested but soon the levenstein strategie
should be added along with the meta strategie.

In order for us to compare them, statistics will be done in the file
observations/global_observations.txt 
