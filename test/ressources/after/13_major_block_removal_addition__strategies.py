# Copyright (C) 2019 Eloïse Brocas, Mert Ernez, Jérôme Faure, Raphaël Gilliot
# Lucas Henry, Yann Julienne, Benjamin Le Rohellec
#
# This file is part of coderev, a code review tool.
#
# coderev is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# coderev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with coderev.  If not, see <https://www.gnu.org/licenses/>.

"""
List of strategies to be applied to anchors in order to updates its data.
"""


import difflib as d
import logging
import unidiff

from unidiff import Hunk


def inside(line, hunk) -> bool:
    """Return whether `ancre` is inside `hunk`
    :rtype: bool
    """
    return (int(line) >= hunk.source_start) and (
        int(line) < (hunk.source_start + hunk.source_length)
    )


def touch(anchor, hunk) -> bool:
    return inside(anchor.lines[0], hunk) or inside(anchor.lines[1], hunk)


class Strategy:
    """
    Base Strategy class. This class does nothing and is not intented to be used directly.
    All strategies should derive from this class.
    """

    def __init__(self, new_filename, old_filename, ANCHORS):
        self.new_filename = new_filename
        self.old_filename = old_filename
        self.ANCHORS = ANCHORS

    def apply(self) -> bool:
        """
        Default apply method. Return a list of new anchors that are copies
        of anchors in ANCHORS.
        """
        return []


class BasicDiffStrategy(Strategy):
    """
    Base class for strategies that use the classic diff system.
    This class does nothing and is not intented to be used directly.
    All strategies using the classic diff should derive from this class.
    """

    def __init__(self, new_filename, old_filename, ANCHORS):
        self.new_filename = new_filename
        self.old_filename = old_filename
        self.ANCHORS = ANCHORS
        self.diff = None
        self.generate_diff()

    def apply(self) -> bool:
        """
        Default apply method. Return a list of new anchors that are copies
        of anchors in ANCHORS.
        TODO: use newly created anchors for next hunks
        """
        anchor_list = self.ANCHORS.anchors
        logging.debug("BasicDiffStrategy: Working on %d anchors", len(anchor_list))

        patch_set = unidiff.PatchSet(self.diff)
        created_anchors = []

        for patch in patch_set:
            source_filename = patch.source_file.split(" ")[0]

            # Computing anchors concerned by the patch
            anchors = [a for a in anchor_list if a.filename == source_filename]

            for hunk in patch:
                logging.debug("BasicDiffStrategy: hunk %s", hunk)
                new_anchors_for_hunk = []
                for anchor in anchors:
                    anchor_created = self.apply_hunk(anchor, hunk)
                    if anchor_created:
                        new_anchors_for_hunk.append(anchor_created)
                created_anchors.extend(new_anchors_for_hunk)

            return created_anchors

    def generate_diff(self):
        "Generate a diff string in the unified format"
        old_content = open(self.old_filename, "r").readlines()
        new_content = open(self.new_filename, "r").readlines()
        unified_diff = d.unified_diff(
            old_content, new_content, self.old_filename, self.new_filename
        )
        self.diff = "".join(list(unified_diff))

    def apply_hunk(self, anchor, hunk):
        """
        Apply the modification of the `hunk` to the `anchor`.
        Return the new anchor if modified, else None.
        """
        return None


class SimpleStrategy(BasicDiffStrategy):
    """
    Simplest strategy. It works on anchor with only one line.
    If the modification is before the anchor, update the line.
    Otherwise, do nothing.
    """

    def __init__(self, new_filename, old_filename, ANCHORS):
        super().__init__(new_filename, old_filename, ANCHORS)

    def apply_hunk(self, anchor, hunk):
        if int(anchor.lines[0]) > hunk.source_length + hunk.source_start:
            logging.debug("%s: Hunk is before anchor: %s", __name__, anchor)
            anchor.lines[0] += hunk.target_length - hunk.source_length
            anchor.lines[1] += hunk.target_length - hunk.source_length
            return True

        moved = True
        lines = list(anchor.lines)
        if inside(lines[0], hunk):
            logging.debug("Hunk contains anchor: %s", anchor)
            print(hunk)
            src_line = [l for l in hunk if l.source_line_no == int(lines[0])][0]

            # line is not modified
            if src_line.target_line_no:
                lines[0] = src_line.target_line_no
            else:
                move = False

        if inside(lines[1], hunk):
            logging.debug("Hunk contains anchor: %s", anchor)
            print(hunk)
            src_line = [l for l in hunk if l.source_line_no == int(lines[1])][0]

            # line is not modified
            if src_line.target_line_no:
                lines[1] = src_line.target_line_no
            else:
                moved = False

        if moved:
            anchor.lines = tuple(lines)
        return moved


class LevensteinStrategy(BasicDiffStrategy):
    """
    Levenstein strategy. Update
    If the modification is before the anchor, update the line.
    Otherwise, do nothing.
    """

    def __init__(self, new_filename, old_filename, ANCHORS, minimum_ratio=0.8):
        super().__init__(new_filename, old_filename, ANCHORS)
        self.min_ratio = minimum_ratio

    def apply_hunk(self, anchor, hunk):
        if inside(anchor, hunk):
            # line has been modified
            src_line = [l for l in hunk if l.source_line_no == int(anchor.line)][0]
            added_lines = [l for l in hunk if (l.is_added or l.is_context)]
            line = LevensteinStrategy.best_match(src_line, added_lines)
            if line:
                logging.debug(
                    "levenstein: best match:\n%40s: %s\n%40s: %s",
                    self.old_filename,
                    src_line.value,
                    self.new_filename,
                    line.value,
                )
                logging.info(
                    "levenstein: line moved from %d to %d",
                    int(lines[0]),
                    line.target_line_no,
                )

                anchor.line = line.target_line_no
                return None  # TODO: create a new anchor
        return None

    @staticmethod
    def best_match(src_line, added_lines):
        """
        Return the best line that match `src_line`
        """
        best_ratio = 0
        best_line = None
        for added_line in added_lines:
            ratio = d.SequenceMatcher(None, src_line.value, added_line.value).ratio()
            if ratio > best_ratio:
                best_ratio = ratio
                best_line = added_line
        return best_line


class InteractiveStrategy(Strategy):
    """
    Interactive strategy. Ask the user for each action.
    """

    def __init__(self, new_filename, old_filename, ANCHORS):
        super().__init__(new_filename, old_filename, ANCHORS)

    def apply(self):
        print("Interactive is not implemented yet, not modifying.")
        return False


# Dictionnary mapping short strategy name to class strategies
strategies_map = {
    "simple": SimpleStrategy,
    "leven": LevensteinStrategy,
    "interact": InteractiveStrategy,
}

default_strategies = [SimpleStrategy]
