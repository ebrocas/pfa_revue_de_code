CODEREV=../coderev
PY=python3

tests:
	$(PY) -m unittest discover -p 'test_cases.py'

init:
	$(CODEREV) add --anchor ressources/before/10_factorize_code_similar_in_two_functions_by_creating_a_new_one.txt 2 5 --annotation --title "Je suis un titre" --tags "IMPORTANT"
	$(CODEREV) add --anchor ressources/before/10_factorize_code_similar_in_two_functions_by_creating_a_new_one.txt 3 6
	$(CODEREV) add --anchor ressources/before/10_factorize_code_similar_in_two_functions_by_creating_a_new_one.txt 6 9
	$(CODEREV) add --anchor ressources/before/10_factorize_code_similar_in_two_functions_by_creating_a_new_one.txt 6 6

	$(CODEREV) add --ref 2 3  --annotation --title "I am a title" --description "I am a description" --tags "TODO" "FIXME"
	$(CODEREV) add --ref 4  --annotation --title "Troisieme annotation" --description "I am a description" --tags "TODO" "FIXME"
	$(CODEREV) display annotation
	$(CODEREV) cat ressources/before/10_factorize_code_similar_in_two_functions_by_creating_a_new_one.txt


test_01:
	$(CODEREV) display
	$(CODEREV) add --anchor ressources/before/01_line_inversion.txt 2 --annotation
	$(CODEREV) display
	$(CODEREV) update ressources/before ressources/after --files 01_line_inversion.txt --dry-run
	$(CODEREV) update -s leven ressources/before ressources/after --files 01_line_inversion.txt --dry-run



clean:
	rm -rf .data
