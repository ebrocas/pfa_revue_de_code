import sys
import unittest
from os.path import dirname

sys.path.append(dirname(dirname(__file__)))
from src.versionning import *


class TestFiles(unittest.TestCase):
    def test_files(self):
        test = dirname(abspath(__file__))
        vp = FileVersionProvider(join(test, "ressources", "before"),
                                 join(test, "ressources", "after"))
        
        with open(join(test, "ressources", "before", "01_line_inversion.txt"), "r") as original:
            with vp.file_content("before", "01_line_inversion.txt") as file:
                self.assertEqual(file.read(), original.read())
                
        with open(join(test, "ressources", "after", "01_line_inversion.txt"), "r") as original:
            with vp.file_content("after", "01_line_inversion.txt") as file:
                self.assertEqual(file.read(), original.read())


class TestGit(unittest.TestCase):
    def test_git(self):
        vp = GitVersionProvider(dirname(__file__))
        
        with open(join(dirname(dirname(__file__)), ".git", "HEAD"), "r", encoding="utf-8") as f:
            head = f.readline()
            if head.endswith("\n"):
                head = head[:-1]
        if head.startswith("ref:"):
            with open(join(dirname(dirname(__file__)), ".git", head[5:]), "r", encoding="utf-8") as f2:
                head = f2.readline()
                if head.endswith("\n"):
                    head = head[:-1]
            
        self.assertEqual(head, vp.actual_version(), "read HEAD")


if __name__ == "__main__":
    unittest.main()