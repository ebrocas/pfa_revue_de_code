#!/usr/bin/env python3
import sys
import unittest
from os import listdir
from os.path import dirname, join

from ddt import ddt, data

sys.path.append(dirname(dirname(__file__)))
from src.updater import *
from src.versionning import FileVersionProvider

logging.basicConfig(level=logging.CRITICAL)


class Case:
    anchorLines = []
    expectedALines = []

    def __init__(self, caseName: str, before_path, after_path, expected_path):
        self.caseNumber: int = int(caseName.split("_")[0])
        self.beforePath = before_path
        self.afterPath = after_path
        self.expectedPath = expected_path
        self.anchorLines = []
        self.expectedALines = []
        self.caseName = caseName

    def __repr__(self):
        return str(self)

    def getName(self):
        return self.caseName

    def getAnchorLines(self):
        return self.anchorLines

    def parseExpectedFile(self):
        F = open(join(self.expectedPath, self.caseName), "r")
        lines = F.readlines()
        for line in lines:
            diff = line.split(" ")
            anchorLine = tuple(int(i) for i in diff[0].split(","))
            expectedALine = tuple(int(i) for i in diff[1].split(","))
            self.expectedALines.append(expectedALine)
            self.anchorLines.append(anchorLine)
        F.close()

    def __str__(self):
        return "<CaseNumber : {} ; CaseName : {}>".format(
            self.caseNumber, self.caseName
        )


def generate_cases(resources_path: str, sort=True):
    """
        Returns a sorted list (by case number) of cases corresponding to each test case in provided directory path
    """
    cases = []
    before = join(resources_path, "before")
    after = join(resources_path, "after")
    expected = join(resources_path, "expectations")
    for case in listdir(join(resources_path, "before")):
        cases.append(Case(case, before, after, expected))
    if sort is True:
        cases.sort(key=lambda x: x.caseNumber, reverse=False)
    return cases


@ddt
class TestDiff(unittest.TestCase):
    strategies = ["simple", "leven"]
    results = []

    @data(*generate_cases(join(dirname(__file__), "ressources")))
    def test_cases(self, case):
        self.simple_test(case)

    def simple_test(self, case: Case):
        ANCHORS = AnchorManager()
        anchor_ids = list()
        case.parseExpectedFile()
        # get all the anchors for this case
        for a in case.getAnchorLines():
            anchor_ids.append(ANCHORS.add(case.caseName, a))
        version_provider = FileVersionProvider(case.beforePath, case.afterPath)
        updater = AnchorUpdater(
            ANCHORS, version_provider, self.strategies, should_save=False
        )
        result = updater.update(("before", "after"), auto=True)
        expected = case.expectedALines
        i = 0
        success_rate = 0
        bumped_anchors = []
        for anchor in result:
            old = ANCHORS.anchors[i]
            best = result[anchor]
            if type(best) == type(None):
                # anchor was not moved
                if old.lines == expected[i]:
                    print("here")
                    success_rate = success_rate + 1
            else:
                bumped_anchors.append(best)
                if best.lines == expected[i]:
                    success_rate = success_rate + 1
            i = i + 1
        TestDiff.results.append((case.caseNumber, success_rate / len(expected)))
        # print("\nSuccess rate for {} : {}".format(case.caseName,success_rate/len(expected)))

    @classmethod
    def tearDownClass(cls):
        print("\n")
        for test, res in cls.results:
            print(f"| {test:4} | {res:03.3f} |")


if __name__ == "__main__":
    unittest.main()
