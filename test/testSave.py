# coding:utf-8


import sys
from os.path import dirname, join
sys.path.append(join(dirname(dirname(__file__)), "src"))
from src import conversion
from src.annotation import AnnotationManager, Annotation, Reference
from src.anchor import AnchorManager

ANCHORS = AnchorManager()
ANNOTATIONS = AnnotationManager()


id = ANCHORS.add(
    join(dirname(dirname(__file__)), ".gitignore"), (1, 3), None)
ref = Reference()
ref.addAnchor(id)
annotation = Annotation(None, ref)
ANNOTATIONS.register_annotation(annotation)
annotation.addTag("FIXME")

ANCHORS.add(
    join(dirname(dirname(__file__)), "requirements.txt"), (), None)


if __name__ == "__main__":
    conversion.save_all(ANCHORS, ANNOTATIONS)

    print("les ancres dans le fichier '.gitignore' sont :", ANCHORS.all_in_file(
        join(dirname(dirname(__file__)), ".gitignore")))

    ANCHORS, ANNOTATIONS = conversion.load_all()
    print(ANCHORS.anchors)
    print(ANNOTATIONS._annotations)