#!/usr/bin/env python3
import sys
from os.path import dirname
import unidiff

sys.path.append(dirname(dirname(__file__)))

from src.anchor import AnchorManager
from src import updater

DIFF = """
--- fichier.txt	2019-12-03 17:40:40.503893532 +0100
+++ fichier2.tx	2019-12-03 17:40:23.670897729 +0100
@@ -2,14 +2,8 @@
 class Line(object):
     "A diff line."

-    def __init__(
-        self,
-        value,
-        line_type,
-        source_line_no=None,
-        target_line_no=None,
-        diff_line_no=None,
-    ):
+    def __init__(self, value, line_type,
+                 source_line_no=None, target_line_no=None, diff_line_no=None):
         super(Line, self).__init__()
         self.source_line_no = source_line_no
         self.target_line_no = target_line_no
@@ -24,13 +18,11 @@
         return "%s%s" % (self.line_type, self.value)

     def __eq__(self, other):
-        return (
-            self.source_line_no == other.source_line_no
-            and self.target_line_no == other.target_line_no
-            and self.diff_line_no == other.diff_line_no
-            and self.line_type == other.line_type
-            and self.value == other.value
-        )
+        return (self.source_line_no == other.source_line_no and
+                self.target_line_no == other.target_line_no and
+                self.diff_line_no == other.diff_line_no and
+                self.line_type == other.line_type and
+                self.value == other.value)

     @property
     def is_added(self):
"""

# Configuration
ANCHORS = AnchorManager()
patch_set = unidiff.PatchSet(DIFF)
filename = "fichier.txt"

# Adding anchors
ANCHORS.add(filename, 17)
ANCHORS.add(filename, 3)
ANCHORS.add(filename, 6)
ANCHORS.anchors = updater.bump_all(DIFF, ANCHORS.anchors)
