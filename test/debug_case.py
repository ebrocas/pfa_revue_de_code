#!/usr/bin/env python3
from test_cases import *
import sys
import re
import random
import string
import copy
from os import listdir
from os.path import join

def get_case(resourcesPath,caseNumber):
    """
        For the given path and case number, returns a corresponding case object
    """
    r = re.compile("^"+caseNumber+"_")
    caseName = list(filter(r.match, listdir(join(resourcesPath,"before/"))))[0]
    before=join(resourcesPath,"before")
    after=join(resourcesPath,"after")
    expected=join(resourcesPath,"expectations")
    return Case(caseName,before,after,expected)

def get_labeled_anchors(anchor_list):
    """
       anchor_lis : list of anchors
       returns : dictionary where the key is the anchor and the value is a randomly generated string (label)
    """
    labeled_anchors=dict()
    for anchor_id in anchor_list:
        #generate random string to label anchors
        random_string = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(4))
        labeled_anchors[copy.deepcopy(anchor_id)]=random_string
    return labeled_anchors

def print_details(case,anchors):
    expected = case.expectedALines
    successes=0
    expected_id = 0
    for label in anchors.items():
        original=label[1][0]
        bumped=label[1][1]
        print(" Anchor {} {} ( {} ) expected {} bumped {} {} ( {} ) state : {}".format(original[0].anchor_id,original[0].lines,label[1][0][1],expected[expected_id],bumped[0].anchor_id,bumped[0].lines,label[1][1][1],bumped[0]._state))
        expected_id=expected_id+1

        
def concat_dict(original,bumped):
    """
      original : dictionary of original anchors
      bumped : dictionary made of the bumped state of the original anchors
      returns: Dict(label anchor X: (original anchor X ,  bumped anchor X))
    """
    res=dict()
    o_l = []
    b_l = []
    labels=[]
    for o in original.items():
        o_l.append(o)
        labels.append(o[1])
    for b in bumped.items():
        b_l.append(b)
    i=0
    for i in range(0,min(len(labels),len(o_l),len(b_l))):
        res[labels[i]]=(o_l[i],b_l[i])
        i=i+1
    return res

def run_case(case,strategies=[]):
    """
        Executes the given test case and displays debugging messages
    """
    print("Cas : " + ''.join(' '.join(case.caseName.split("_")[1:]).split(".")[:-1]))
    ANCHORS = AnchorManager()
    anchor_ids = list()
    case.parseExpectedFile()
    # get all the anchors for this case
    for a in case.getAnchorLines():
        anchor_ids.append(ANCHORS.add(case.caseName, a))
    # copy anchor list f3rom manager and label each anchor
    labeled_anchors = get_labeled_anchors(ANCHORS.anchors)
    print("************Original file content************")
    version_provider = FileVersionProvider(case.beforePath, case.afterPath)
    ANCHORS.cat_anchors(labeled_anchors, case.caseName,  version_provider, "before")

    updater = AnchorUpdater(ANCHORS, version_provider, strategies, False)
    result = updater.update(("before","after"),True)
    expected = case.expectedALines
    i = 0
    success_rate = 0
    bumped_anchors = []
    for anchor in result:
        old = ANCHORS.anchors[i]
        best = result[anchor]
        if(type(best) == type(None)):
            #anchor was not moved
            if(old.lines == expected[i]):
                success_rate = success_rate+1
        else:
            bumped_anchors.append(best)
            if(best.lines == expected[i]):
                success_rate = success_rate+1
        print("Anchor {} at {} expected at {} : best candidate id : {} at {}".format(old.anchor_id,old.lines,expected[i],best.anchor_id,best.lines))
        i=i+1
    print("\n\n************Altered file content************")
    labeled_bumped_anchors = get_labeled_anchors(bumped_anchors)
    ANCHORS.cat_anchors(labeled_bumped_anchors, case.caseName, version_provider, "after")
    print("Success rate : {}".format(success_rate/len(expected)))
    
def filterFileName(name):
    return name.split("_")[0]

def minMax():
    values=list(map(filterFileName,listdir("test/ressources/before")))
    return (min(values),max(values))

def caseExists(caseNumber):
    (m,M) = minMax()
    if caseNumber < m or caseNumber > M:
        return False
    return True

if(len(sys.argv) == 2 and sys.argv[1].isdigit() and caseExists(sys.argv[1])):
    run_case(get_case("test/ressources/",sys.argv[1]),['simple','leven'])
else:
    sys.exit("Error: Only one argument which must be the number of the test case in range {}".format(minMax()))

  
