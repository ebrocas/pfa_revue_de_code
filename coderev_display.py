# coding:utf8
import argparse

from src import conversion
from src.annotation import Annotation
from src.versionning import VersionProvider, parse_ref


def create_subparser_display(parser):
    sub = parser.add_subparsers(dest="display_action")
    
    annotation = sub.add_parser("annotation")
    annotation.add_argument("--all", "-a", action="store_true", default=False, help="Display all available, restricted by filters.")
    annotation.add_argument("--file", "-f", nargs="+", metavar="file", help="restrict to annotations present in a files.")
    annotation.add_argument("--line", "-l", nargs="+", type=int, metavar="line",
                            help="restrict to annotations present in a file at a specified line.")
    annotation.add_argument("--version", "-v", type=str, help="Select a different version from the current version")
    annotation.add_argument("id", nargs="*", help="Select an annotation by its id.")
    
    reference = sub.add_parser("anchor")
    reference.add_argument("--all", "-a", action="store_true", help="Display all available, restricted by filters.")
    reference.add_argument("--file", "-f", nargs="+", metavar="file", help="restrict to anchors present in a files.")
    reference.add_argument("--line", "-l", nargs="+", type=int, metavar="line",
                            help="restrict to annotations present in a file at a specified line.")
    reference.add_argument("--version", "-v", type=str, help="Select a different version from the current version")
    reference.add_argument("id", nargs="*", help="Select an anchor by its id.")


def execution(args: argparse.Namespace, version_manager: VersionProvider):
    version_id = parse_ref(version_manager, args.version)
    if args.display_action == "annotation":
        ANNOTATIONS = conversion.load_annotations()
        if args.id is not None and len(args.id) > 0:
            for id in map(int, args.id):
                if id in ANNOTATIONS:
                    ann: Annotation = ANNOTATIONS.get(id)
                    print(ann.id, ":", ann.title)
                    print("Tags:", ", ".join(ann.tags))
                    print("Anchors:", repr(ann.reference))
                    if ann.description is not None:
                        print(ann.description)
                    print()
                else:
                    print("'{:d}' is not a valid id.".format(id))
        
        else:
            list_id = ANNOTATIONS.get_ids()
            if args.all is False:
                list_id = (i for i in list_id if ANNOTATIONS.get(i).open)
            if args.file:
                ANCHORS = conversion.load_anchors(version_id)
                files = set(args.file)
                list_id = (i for i in list_id if len(files.intersection(set(ANNOTATIONS.get(i).files(ANCHORS)))) > 0)
            
            for id in list_id:
                ann = ANNOTATIONS.get(id)
                print("{:d} {:s}".format(id, ann.title if ann.title else "None"))

    if args.display_action == "anchor":
        ANCHORS = conversion.load_anchors(version_id)
        for anchor in ANCHORS.anchors:
            print(repr(anchor))
